// Copyright (C) 2014  Pitzik4
// Want to see the full notice of legal stuff? It's in notice.txt.

package net.pitzik4.gotile;

import java.awt.Dimension;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JPanel;

import java.awt.Desktop;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Color;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import javax.swing.JTextPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JCheckBox;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JSlider;
import javax.swing.JLabel;

import com.google.gson.Gson;
import com.jgoodies.forms.factories.DefaultComponentFactory;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.filechooser.FileFilter;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.CardLayout;
import javax.swing.JTextField;
import javax.swing.BoxLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class GoTileUI extends JFrame {
	private static final Logger LOGGER = Logger.getLogger(GoTileUI.class.getName());
	
	private static final long serialVersionUID = 1528489377691188817L;
	public static final Dimension FRAME_SIZE = new Dimension(800,600);
	public static final String[] EX_LEVEL = new String[12];
	{
		InputStream is = getClass().getResourceAsStream("/example.csv");
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String line = "";
		try {
			int i = 0;
			while((line = br.readLine()) != null) {
				EX_LEVEL[i++] = line;
			}
		} catch (IOException e) {
			LOGGER.severe(e.getMessage());
			LOGGER.severe("IOException while loading example level!");
		}
	}
	
	private ImagePicker ipTil, ipTop, ipBot, ipRgt, ipLft, ipMsk, ipBG;
	public JCheckBox interactive, topperLayer;
	public BufferedImage preview;
	private JPanel previewPanel;
	public JSlider arcSlider;
	public JComboBox<String> outputBox;
	private JTextField txtChooseLevelFile;
	public String[] level = {"0"};
	private CardLayout wolcards, bgcards;
	public JSlider widthSlider;
	public JComboBox<String> formatBox;
	public String levelLoc = "";
	private JTextField txtBGColor;
	private JTextField txtChooseBGTile;
	public Object background = null;
	public Image bgImage = null;
	public JComboBox<String> bgTypeBox = null;
	public JPanel colorPreview;
	
	public GoTileUI() {
		super("GoTile");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setPreferredSize(FRAME_SIZE);
		setIconImage(new ImageIcon(getClass().getResource("/icon.png")).getImage());
		ipTil = new ImagePicker(this); ipTop = new ImagePicker(this);
		ipBot = new ImagePicker(this); ipRgt = new ImagePicker(this);
		ipLft = new ImagePicker(this); ipMsk = new ImagePicker(this);
		
		initUI();
		interactive.setSelected(true);
		
		pack();
		setLocationRelativeTo(null);
		
		new Thread() {
			@Override
			public void run() {
				new UpdateWindow().openIfYouWant(GoTileUI.this);
			}
		}.start();
	}
	private void initUI() {
		JPanel tileSelections = new JPanel();
		
		final JTextPane txtpnLogMessages = new JTextPane();
		txtpnLogMessages.setEditable(false);
		txtpnLogMessages.setToolTipText("Last logged message");
		txtpnLogMessages.setBackground(new Color(0, 0, 0, 0));
		txtpnLogMessages.setForeground(Color.BLACK);
		//txtpnLogMessages.setText("Log messages");
		Logger.getLogger("net.pitzik4.gotile").addHandler(new Handler() {
			@Override
			public void publish(LogRecord record) {
				txtpnLogMessages.setText(record.getMessage());
				txtpnLogMessages.setForeground(record.getLevel().intValue() > Level.INFO.intValue() ? Color.RED : Color.BLACK);
			}
			@Override
			public void flush() {  }
			@Override
			public void close() throws SecurityException {
				txtpnLogMessages.setText(null);
			}
		});
		
		previewPanel = new JPanel() {
			private static final long serialVersionUID = -6927007011270747747L;
			@Override
			public void paint(Graphics g) {
				final int width = getWidth(), height = getHeight();
				g.setColor(Color.BLACK);
				g.fillRect(0, 0, width, height);
				if(preview == null) {
					return;
				}
				final int iw = preview.getWidth(), ih = preview.getHeight();
				if(iw > width || ih > height) {
					int sx = (int) Math.ceil(((float) iw)/(float) width); int sy = (int) Math.ceil(((float) ih)/(float) height);
					int scale = Math.max(sx, sy);
					int x = (width-iw/scale)/2, y = (height-ih/scale)/2;
					g.drawImage(preview, x, y, iw/scale, ih/scale, null);
				} else {
					int sx = (int) width/iw; int sy = (int) height/ih;
					int scale = Math.min(sx, sy);
					int x = (width-iw*scale)/2, y = (height-ih*scale)/2;
					g.drawImage(preview, x, y, iw*scale, ih*scale, null);
				}
			}
		};
		previewPanel.setForeground(Color.LIGHT_GRAY);
		previewPanel.setBackground(Color.BLACK);
		previewPanel.setToolTipText("Preview area");
		
		interactive = new JCheckBox("Interactive Preview");
		interactive.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if(interactive.isSelected()) {
					new Thread(new InteractivePreviewer(GoTileUI.this)).start();
				}
			}
		});
		
		JButton btnUpdate = new JButton("Update");
		btnUpdate.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				beginUpdatePreview();
			}
		});
		
		final JLabel lblArcSize = DefaultComponentFactory.getInstance().createLabel("Arc Size (12)");
		lblArcSize.setHorizontalAlignment(SwingConstants.CENTER);
		
		arcSlider = new JSlider();
		arcSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				if(arcSlider.getValue() % 2 == 1) {
					arcSlider.setValue((arcSlider.getValue() / 2) * 2);
				}
				lblArcSize.setText("Arc Size (" + arcSlider.getValue() + ")");
			}
		});
		arcSlider.setSnapToTicks(true);
		arcSlider.setMinorTickSpacing(2);
		arcSlider.setValue(12);
		arcSlider.setMaximum(32);
		
		JLabel lblOutput = DefaultComponentFactory.getInstance().createLabel("Output:");
		
		final JPanel widthOrLevel = new JPanel();
		
		outputBox = new JComboBox<String>();
		outputBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String value = (String) outputBox.getSelectedItem();
				if(value.equals("example level")) {
					level = EX_LEVEL;
					wolcards.show(widthOrLevel, "empty");
				} else if(value.equals("custom level")) {
					wolcards.show(widthOrLevel, "level");
				} else {
					wolcards.show(widthOrLevel, "tileset");
					if(value.startsWith("48")) {
						widthSlider.setValue(8);
						widthSlider.setMaximum(48);
					} else if(value.startsWith("16")) {
						widthSlider.setValue(4);
						widthSlider.setMaximum(16);
					} else if(value.startsWith("256")) {
						widthSlider.setValue(16);
						widthSlider.setMaximum(256);
					}
				}
			}
		});
		outputBox.setModel(new DefaultComboBoxModel<String>(new String[] {"48-tile set", "16-tile set", "256-tile set (redundant!)", "example level", "custom level"}));
		
		final JFileChooser exportFC = new JFileChooser();
		exportFC.setFileFilter(new ImagePicker.ImageFileFilter());
		JButton btnExport = new JButton("Export Image");
		btnExport.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(exportFC.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
					updatePreview();
					try {
						String format = (String) formatBox.getSelectedItem();
						ImageIO.write(preview, format, perhapsAddExtension(exportFC.getSelectedFile(), format.toLowerCase()));
					} catch (IOException exc) {
						LOGGER.severe(exc.getMessage());
						LOGGER.severe("Could not export image!");
					}
				}
			}
		});
		
		formatBox = new JComboBox<String>();
		formatBox.setModel(new DefaultComboBoxModel<String>(unique(ImageIO.getWriterFormatNames())));
		formatBox.setSelectedItem("PNG");
		
		JButton btnInfo = new JButton("Info");
		btnInfo.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				JFrame infoFrame = new JFrame("GoTile - Info");
				infoFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				JPanel cont = new JPanel();
				infoFrame.getContentPane().add(cont);
				cont.setBorder(new EmptyBorder(8,8,8,8));
				cont.setLayout(new GridLayout(0, 1, 0, 0));
				
				JLabel lblPurpose = DefaultComponentFactory.getInstance().createLabel("GoTile automatically generates tilesets from a few images.");
				lblPurpose.setFont(new Font("Dialog", Font.PLAIN, 12));
				cont.add(lblPurpose);
				
				JLabel lblCopyright = DefaultComponentFactory.getInstance().createLabel("GoTile v" + GoTile.VERSTRING + " is Copyright (C) 2014  Pitzik4, under a GPLv3 license.");
				lblCopyright.setFont(new Font("Dialog", Font.PLAIN, 12));
				cont.add(lblCopyright);
				
				JPanel linkntext = new JPanel();
				cont.add(linkntext);
				linkntext.setLayout(new BoxLayout(linkntext, BoxLayout.X_AXIS));
				
				JLabel lblInspiredBy = DefaultComponentFactory.getInstance().createLabel("Inspired by Javi Cepa's ");
				linkntext.add(lblInspiredBy);
				lblInspiredBy.setFont(new Font("Dialog", Font.PLAIN, 12));
				
				JLabel lblAutotilegen = DefaultComponentFactory.getInstance().createLabel("AutoTileGen.");
				lblAutotilegen.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e1) {
						if(Desktop.isDesktopSupported()) {
							try {
								Desktop.getDesktop().browse(new URI("http://autotilegen.com/"));
							} catch (IOException | URISyntaxException exc) {
								LOGGER.severe(exc.getMessage());
								LOGGER.severe("Error opening link");
							}
						}
					}
				});
				lblAutotilegen.setForeground(Color.BLUE);
				linkntext.add(lblAutotilegen);
				infoFrame.pack();
				infoFrame.setLocationRelativeTo(GoTileUI.this);
				infoFrame.setVisible(true);
			}
		});
		
		topperLayer = new JCheckBox("Topper layered on top");
		
		final JFileChooser saveFC = new JFileChooser();
		saveFC.setFileFilter(new SaveFileFilter());
		JButton btnSave = new JButton("Save");
		btnSave.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(saveFC.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
					save(perhapsAddExtension(saveFC.getSelectedFile(), "gtl"));
				}
			}
		});
		
		final JFileChooser loadFC = saveFC;
		loadFC.setFileFilter(new SaveFileFilter());
		JButton btnLoad = new JButton("Load");
		btnLoad.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(loadFC.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
					applySaveData(readSave(loadFC.getSelectedFile()));
				}
			}
		});
		
		JLabel lblBG = DefaultComponentFactory.getInstance().createLabel("Background:");
		
		colorPreview = new JPanel() {
			private static final long serialVersionUID = 2617965147859709397L;
			public int lastColor = 0xFF000000;
			
			@Override
			public void paint(Graphics g) {
				int color = lastColor;
				try {
					String col = txtBGColor.getText();
					if(col.charAt(0) == '#') col = col.substring(1);
					if(col.length() != 6) throw new Exception();
					color = Integer.parseInt(col, 16) | 0xFF000000;
				} catch(Exception e) {
					color = lastColor;
				}
				Color col = new Color(lastColor = color);
				if(GoTileUI.this.background instanceof Color)
					GoTileUI.this.background = col;
				g.setColor(col);
				g.fillRect(0, 0, this.getWidth(), this.getHeight());
			}
		};
		final JPanel bgOptPanel = new JPanel();
		bgcards = new CardLayout(0, 0);
		bgOptPanel.setLayout(bgcards);
		bgTypeBox = new JComboBox<String>();
		bgTypeBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String value = (String) bgTypeBox.getSelectedItem();
				if(value.equals("None")) {
					bgcards.show(bgOptPanel, "empty");
					GoTileUI.this.background = null;
				} else if(value.equals("Solid color")) {
					bgcards.show(bgOptPanel, "color");
					GoTileUI.this.background = Color.BLACK;
					colorPreview.repaint();
				} else {
					bgcards.show(bgOptPanel, "tile");
					background = bgImage;
				}
			}
		});
		bgTypeBox.setModel(new DefaultComboBoxModel<String>(new String[] {"None", "Solid color", "Tile"}));
		bgTypeBox.setSelectedIndex(0);
		
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(tileSelections, GroupLayout.PREFERRED_SIZE, 216, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(previewPanel, GroupLayout.DEFAULT_SIZE, 671, Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(txtpnLogMessages, GroupLayout.DEFAULT_SIZE, 671, Short.MAX_VALUE)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(btnLoad)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnSave)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnInfo))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(lblBG)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(bgTypeBox, 0, 0, Short.MAX_VALUE))
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(12)
									.addComponent(lblArcSize, GroupLayout.DEFAULT_SIZE, 188, Short.MAX_VALUE))
								.addComponent(arcSlider, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(topperLayer))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(lblOutput)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(outputBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
								.addComponent(widthOrLevel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(bgOptPanel, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
							.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(btnUpdate)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(interactive))
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(formatBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(btnExport)))))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(previewPanel, GroupLayout.PREFERRED_SIZE, 384, GroupLayout.PREFERRED_SIZE)
						.addComponent(tileSelections, GroupLayout.PREFERRED_SIZE, 384, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(outputBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblOutput))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(widthOrLevel, GroupLayout.PREFERRED_SIZE, 37, GroupLayout.PREFERRED_SIZE))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(arcSlider, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblArcSize)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(topperLayer))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(interactive)
								.addComponent(btnUpdate))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
								.addComponent(btnExport)
								.addComponent(formatBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(bgOptPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
						.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
							.addGroup(groupLayout.createSequentialGroup()
								.addGap(15)
								.addComponent(bgTypeBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGroup(groupLayout.createSequentialGroup()
								.addPreferredGap(ComponentPlacement.RELATED)
								.addComponent(lblBG))))
					.addPreferredGap(ComponentPlacement.RELATED, 24, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
							.addComponent(btnInfo)
							.addComponent(btnSave)
							.addComponent(btnLoad))
						.addComponent(txtpnLogMessages, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addContainerGap())
		);
		
		JPanel bgColor = new JPanel();
		bgOptPanel.add(bgColor, "color");
		bgColor.setLayout(null);
		
		txtBGColor = new JTextField();
		txtBGColor.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				colorPreview.repaint();
			}
		});
		txtBGColor.setBounds(0, 0, 76, 30);
		txtBGColor.setText("#000000");
		bgColor.add(txtBGColor);
		txtBGColor.setColumns(10);
		
		colorPreview.setBounds(76, 0, 30, 30);
		bgColor.add(colorPreview);
		
		JPanel bgTile = new JPanel();
		bgOptPanel.add(bgTile, "tile");
		bgTile.setLayout(new BoxLayout(bgTile, BoxLayout.X_AXIS));
		
		ipBG = new ImagePicker(this);
		JButton btnPick = new JButton(" ");
		btnPick.addMouseListener(ipBG);
		btnPick.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				txtChooseBGTile.setText(ipBG.imgloc);
				GoTileUI.this.background = ipBG.img;
				bgImage = ipBG.img;
			}
		});
		bgTile.add(btnPick);
		
		txtChooseBGTile = new JTextField();
		txtChooseBGTile.setEditable(false);
		txtChooseBGTile.setText("choose repeating tile");
		bgTile.add(txtChooseBGTile);
		txtChooseBGTile.setColumns(10);
		
		JPanel bgEmpty = new JPanel();
		bgOptPanel.add(bgEmpty, "empty");
		widthOrLevel.setLayout(wolcards = new CardLayout(0, 0));
		
		bgcards.show(bgOptPanel, "empty");
		
		JPanel widthPanel = new JPanel();
		widthOrLevel.add(widthPanel, "tileset");
		widthPanel.setLayout(new GridLayout(0, 1, 0, 0));
		
		final JLabel lblWidth = DefaultComponentFactory.getInstance().createLabel("Width of Tileset (in tiles)");
		lblWidth.setHorizontalAlignment(SwingConstants.CENTER);
		widthPanel.add(lblWidth);
		
		widthSlider = new JSlider();
		widthSlider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				lblWidth.setText("Width of Tileset (in tiles) (" + widthSlider.getValue() + ")");
			}
		});
		widthSlider.setValue(8);
		widthSlider.setMinorTickSpacing(1);
		widthSlider.setMinimum(1);
		widthSlider.setMaximum(48);
		widthPanel.add(widthSlider);
		
		JPanel levelPanel = new JPanel();
		widthOrLevel.add(levelPanel, "level");
		levelPanel.setLayout(new BoxLayout(levelPanel, BoxLayout.X_AXIS));
		
		final JFileChooser levelFC = new JFileChooser();
		JButton levelSelectButton = new JButton(" ");
		levelSelectButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(levelFC.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
					try {
						level = Files.readAllLines(levelFC.getSelectedFile().toPath(), Charset.defaultCharset()).toArray(new String[0]);
					} catch (IOException exc) {
						LOGGER.severe(exc.getMessage());
						LOGGER.severe("Could not read file " + levelFC.getSelectedFile().getName());
						return;
					}
					levelLoc = levelFC.getSelectedFile().getAbsolutePath();
					txtChooseLevelFile.setText(levelLoc);
				}
			}
		});
		levelPanel.add(levelSelectButton);
		
		txtChooseLevelFile = new JTextField();
		txtChooseLevelFile.setEditable(false);
		txtChooseLevelFile.setText("choose level file");
		levelPanel.add(txtChooseLevelFile);
		txtChooseLevelFile.setColumns(10);
		
		JPanel emptyPanel = new JPanel();
		widthOrLevel.add(emptyPanel, "empty");
		tileSelections.setLayout(new GridLayout(3, 0, 0, 0));
		
		JButton btnMainTile = new JButton("Main Tile");
		btnMainTile.addMouseListener(ipTil.setButton(btnMainTile));
		btnMainTile.setBackground(Color.GRAY);
		tileSelections.add(btnMainTile);
		
		JButton btnTopper = new JButton("Topper");
		btnTopper.addMouseListener(ipTop.setButton(btnTopper));
		btnTopper.setBackground(Color.GRAY);
		tileSelections.add(btnTopper);
		
		JButton btnBottom = new JButton("Bottom");
		btnBottom.addMouseListener(ipBot.setButton(btnBottom));
		btnBottom.setBackground(Color.GRAY);
		tileSelections.add(btnBottom);
		
		JButton btnLeft = new JButton("Left");
		btnLeft.addMouseListener(ipLft.setButton(btnLeft));
		btnLeft.setBackground(Color.GRAY);
		tileSelections.add(btnLeft);
		
		JButton btnRight = new JButton("Right");
		btnRight.addMouseListener(ipRgt.setButton(btnRight));
		btnRight.setBackground(Color.GRAY);
		tileSelections.add(btnRight);
		
		JButton btnMask = new JButton("Mask");
		btnMask.addMouseListener(ipMsk.setButton(btnMask));
		btnMask.setBackground(Color.GRAY);
		tileSelections.add(btnMask);
		getContentPane().setLayout(groupLayout);
	}
	public void updatePreview() {
		if(ipTil.img == null) {
			if(!interactive.isSelected())
				LOGGER.warning("No main tile specified.");
			return;
		}
		if(ipTop.img == null) {
			if(!interactive.isSelected())
				LOGGER.warning("No topper specified.");
			return;
		}
		SheetBuilder sb = SheetBuilder.makeSheetBuilder(ipTil.img, ipTop.img, ipBot.img, ipRgt.img, ipLft.img, ipMsk.img, arcSlider.getValue(), background, topperLayer.isSelected());
		int amttiles = 48;
		String it = (String) outputBox.getSelectedItem();
		if(it.startsWith("48")) {
			amttiles = 48;
		} else if(it.startsWith("16")) {
			amttiles = 16;
		} else if(it.startsWith("256")) {
			amttiles = 256;
		}
		if(it.endsWith("level")) {
			preview = sb.level(level);
		} else {
			preview = sb.tileset(amttiles, widthSlider.getValue());
		}
		previewPanel.repaint();
	}
	public void beginUpdatePreview() {
		new Thread() {
			@Override
			public void run() {
				updatePreview();
			}
		}.start();
	}
	public static String[] unique(String[] strings) {
        Set<String> set = new HashSet<String>();
        for (int i = 0; i < strings.length; i++) {
            String name = strings[i].toUpperCase();
            if(name.equals("JPG")) {
            	name = "JPEG";
            } else if(name.equals("TIF")) {
            	name = "TIFF";
            }
            set.add(name);
        }
        return set.toArray(new String[0]);
    }
	public String getSaveData() {
		Gson g = new Gson();
		return g.toJson(new SaveData(this));
	}
	public void save(File saveFile) {
		FileOutputStream os = null;
		try {
			os = new FileOutputStream(saveFile);
		} catch (FileNotFoundException e) {
			LOGGER.severe("File " + saveFile.getName() + " does not exist!");
			return;
		}
		OutputStreamWriter ow = new OutputStreamWriter(os);
		try {
			ow.append(getSaveData());
		} catch (IOException e) {
			LOGGER.severe("Could not write to file!");
		}
		try {
			ow.close();
			os.close();
		} catch (IOException e) {
			LOGGER.severe("Could not close stream!");
		}
	}
	public static SaveData readSave(File f) {
		StringBuilder buf = new StringBuilder();
		FileInputStream is = null;
		try {
			is = new FileInputStream(f);
		} catch (FileNotFoundException e) {
			LOGGER.severe("File " + f.getName() + " does not exist!");
			return null;
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(is));
		String line;
		try {
			while((line = br.readLine()) != null) {
				buf.append(line);
			}
		} catch (IOException e) {
			LOGGER.severe("Problem reading file!");
		}
		try {
			br.close();
			is.close();
		} catch (IOException e) {
			LOGGER.severe("Could not close stream!");
		}
		Gson g = new Gson();
		try {
			return g.fromJson(buf.toString(), SaveData.class);
		} catch (Exception e) {
			LOGGER.severe("Invalid JSON!");
			return null;
		}
	}
	public void applySaveData(SaveData sd) {
		if(sd == null) return;
		ipTil.loadFile(sd.til); ipTop.loadFile(sd.top);
		ipRgt.loadFile(sd.rgt); ipBot.loadFile(sd.bot);
		ipLft.loadFile(sd.lft); ipMsk.loadFile(sd.msk);
		if(sd.lvl != null && sd.lvl.length() > 0) {
			try {
				level = Files.readAllLines(Paths.get(sd.lvl), Charset.defaultCharset()).toArray(new String[0]);
				levelLoc = sd.lvl;
			} catch (IOException e) {
				LOGGER.severe("Could not read level file!");
			}
		}
		topperLayer.setSelected(sd.topperLayer);
		arcSlider.setValue(sd.arcSize);
		widthSlider.setValue(sd.width);
		outputBox.setSelectedItem(sd.output);
		if(sd.background == null || sd.background.length() <= 0) {
			bgTypeBox.setSelectedIndex(0);
		} else if(sd.background.charAt(0) == '#') {
			bgTypeBox.setSelectedIndex(1);
			txtBGColor.setText(sd.background.substring(1));
			colorPreview.repaint();
		} else if(sd.background.charAt(0) == ':') {
			bgTypeBox.setSelectedIndex(2);
			ipBG.loadFile(sd.background.substring(1));
			txtChooseBGTile.setText(ipBG.imgloc);
			background = ipBG.img;
			bgImage = ipBG.img;
		}
	}
	public static class SaveData {
		public String til, top, rgt, bot, lft, msk, lvl;
		public boolean topperLayer;
		public int arcSize, width;
		public String output;
		public String background;
		
		public SaveData() {  }
		public SaveData(GoTileUI o) {
			til = o.ipTil.imgloc; top = o.ipTop.imgloc;
			rgt = o.ipRgt.imgloc; bot = o.ipBot.imgloc;
			lft = o.ipLft.imgloc; msk = o.ipMsk.imgloc;
			lvl = o.levelLoc;
			topperLayer = o.topperLayer.isSelected();
			arcSize = o.arcSlider.getValue();
			width = o.widthSlider.getValue();
			output = (String) o.outputBox.getSelectedItem();
			if(o.background == null) {
				background = "";
			} else if(o.background instanceof Color) {
				background = "#" + o.txtBGColor.getText();
			} else {
				background = ":" + o.ipBG.imgloc;
			}
		}
	}
	public static class SaveFileFilter extends FileFilter {
		@Override
		public boolean accept(File f) {
			if(f.isDirectory()) return true;
			int ind = f.getName().lastIndexOf('.');
			if(ind < 0) return false;
			String ext = f.getName().substring(ind+1).toLowerCase();
			return ext.equals("gtl");
		}
		@Override
		public String getDescription() {
			return "GoTile Saves";
		}
	}
	public static File perhapsAddExtension(File f, String ext) {
		if(f.getName().toLowerCase().endsWith("." + ext)) {
			return f;
		} else {
			return new File(f + "." + ext);
		}
	}
}
