// Copyright (C) 2014  Pitzik4
// Want to see the full notice of legal stuff? It's in notice.txt.

package net.pitzik4.gotile;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Logger;

public class SheetBuilder {
	private static final Logger LOGGER = Logger.getLogger(SheetBuilder.class.getName());
	
	public BufferedImage mainImg, topper, bottom, right, left;
	public BufferedImage mask;
	public BufferedImage[] toppers;
	public BufferedImage diaMaskT, diaMaskR, diaMaskB, diaMaskL;
	public BufferedImage[] diaMasks;
	public BufferedImage[] corners;
	public boolean useMask, topperLayer;
	public Color bgColor = null;
	public Image bgImage = null;	
	private int alTop, alRgt, alBot, alLft;
	private Rectangle[] alRects;
	private int[] alLines;
	public static int[] tiles48;
	{
		tiles48 = new int[47];
		BufferedReader reader = null;
		reader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/tiles48")));
		String line = "";
		int i = 0;
		try {
			while((line = reader.readLine()) != null) {
				tiles48[i] = Integer.parseInt(line, 2);
				i++;
			}
		} catch (IOException e) {
			LOGGER.severe(e.getMessage());
			LOGGER.severe("IOException while reading tiles48!");
		}
	}
	public static SheetBuilder makeSheetBuilder(Image img1, Image topper, Image bottom, Image right, Image left, BufferedImage mask, int arc, Object bg) {
		return makeSheetBuilder(img1, topper, bottom, right, left, mask, arc, bg, false);
	}
	public static SheetBuilder makeSheetBuilder(Image img1, Image topper, Image bottom, Image right, Image left, BufferedImage mask, int arc, Object bg, boolean topperLayer) {
		boolean dontDoSides = bottom == null || right == null || left == null;
		SheetBuilder out;
		if(mask == null) {
			if(arc == 0) {
				if(dontDoSides) {
					out = new SheetBuilder(img1, topper);
				} else {
					out = new SheetBuilder(img1, topper, bottom, right, left);
				}
			} else {
				if(dontDoSides) {
					out = new SheetBuilder(img1, topper, arc);
				} else {
					out = new SheetBuilder(img1, topper, bottom, right, left, arc);
				}
			}
		} else {
			if(dontDoSides) {
				out = new SheetBuilder(img1, topper, mask);
			} else {
				out = new SheetBuilder(img1, topper, bottom, right, left, mask);
			}
		}
		if(dontDoSides) {
			if(right!=null&&left!=null) {
				out.right = toBufImg(right);
				out.left = toBufImg(left);
			} else if(right != null) {
				out.right = toBufImg(right);
				out.left = flippedHoriz(right);
			} else if(left != null) {
				out.left = toBufImg(left);
				out.right = flippedHoriz(left);
			}
			if(bottom != null) {
				out.bottom = toBufImg(bottom);
			}
		}
		out.toppers = new BufferedImage[] {out.topper, out.right, out.bottom, out.left};
		out.topperLayer = topperLayer;
		out.calculateAlphaLines();
		out.makeCorners();
		if(bg instanceof Color) {
			out.bgColor = (Color) bg;
		} else if(bg instanceof Image) {
			out.bgImage = (Image) bg;
		}
		return out;
	}
	
	private SheetBuilder(int width, int height) {
		diaMaskT = whiteSlice(width, height, 0);
		diaMaskR = whiteSlice(width, height, 1);
		diaMaskB = whiteSlice(width, height, 2);
		diaMaskL = whiteSlice(width, height, 3);
		diaMasks = new BufferedImage[] {diaMaskT, diaMaskR, diaMaskB, diaMaskL};
	}
	private SheetBuilder(Image mainImg, Image topper) {
		this(mainImg.getWidth(null), mainImg.getHeight(null));
		this.mainImg = toBufImg(mainImg);
		this.topper = toBufImg(topper);
		right = rotated90(topper);
		bottom = rotated90(right);
		left = rotated90(bottom);
		toppers = new BufferedImage[] {this.topper, right, bottom, left};
		useMask = false;
	}
	private SheetBuilder(Image mainImg, Image topper, BufferedImage mask) {
		this(mainImg, topper);
		this.mask = mask;
		useMask = true;
	}
	private SheetBuilder(Image mainImg, Image topper, Image bottom, Image right, Image left, BufferedImage mask) {
		this(mainImg, topper, bottom, right, left);
		this.mask = mask;
		useMask = true;
	}
	private SheetBuilder(Image mainImg, Image topper, Image bottom, Image right, Image left) {
		this(mainImg.getWidth(null), mainImg.getHeight(null));
		this.mainImg = toBufImg(mainImg);
		this.topper = toBufImg(topper);
		this.right = toBufImg(right);
		this.bottom = toBufImg(bottom);
		this.left = toBufImg(left);
		toppers = new BufferedImage[] {this.topper, this.right, this.bottom, this.left};
		useMask = false;
	}
	private SheetBuilder(Image mainImg, Image topper, Image bottom, Image right, Image left, int arc) {
		this(mainImg, topper, bottom, right, left);
		mask = roundRect(mainImg.getWidth(null), mainImg.getHeight(null), arc);
		useMask = true;
	}
	private SheetBuilder(Image mainImg, Image topper, int arc) {
		this(mainImg, topper);
		mask = roundRect(mainImg.getWidth(null), mainImg.getHeight(null), arc);
		useMask = true;
	}
	
	public static BufferedImage roundRect(int width, int height, int arc) {
		BufferedImage out = newBI(width, height);
		Graphics g = out.getGraphics();
		g.setColor(Color.WHITE);
		g.fillRoundRect(0, 0, width-1, height-1, arc, arc);
		g.drawRoundRect(0, 0, width-1, height-1, arc, arc);
		g.dispose();
		return out;
	}
	// Masks by blitting... This is blitting, right?
	public static BufferedImage applyMask(BufferedImage img, BufferedImage mask, Rectangle region) {
		int[] imgdata = new int[region.width];
		int[] maskdata = new int[region.width];
		for(int y = region.y; y < region.y+region.height; y++) {
			img.getRGB(region.x, y, region.width, 1, imgdata, 0, 1);
			mask.getRGB(region.x, y, region.width, 1, maskdata, 0, 1);
			for(int x = 0; x < region.width; x++) {
				imgdata[x] &= maskdata[x];
			}
			img.setRGB(region.x, y, region.width, 1, imgdata, 0, 1);
		}
		return img;
	}
	public static BufferedImage applyMask(BufferedImage img, BufferedImage mask) {
		return applyMask(img, mask, new Rectangle(0, 0, img.getWidth(), img.getHeight()));
	}
	public static BufferedImage duplicate(Image img) {
		BufferedImage out = newBI(img.getWidth(null), img.getHeight(null));
		Graphics g = out.getGraphics();
		g.drawImage(img, 0, 0, null);
		g.dispose();
		return out;
	}
	public static BufferedImage toBufImg(Image img) {
		if(img instanceof BufferedImage) {
			return (BufferedImage) img;
		} else {
			return duplicate(img);
		}
	}
	public static BufferedImage masked(Image img, BufferedImage mask, Rectangle region) {
		BufferedImage out = duplicate(img);
		applyMask(out, mask, region);
		return out;
	}
	public static BufferedImage masked(Image img, BufferedImage mask) {
		return masked(img, mask, new Rectangle(0, 0, img.getWidth(null), img.getHeight(null)));
	}
	public static BufferedImage invertMask(BufferedImage mask, Rectangle nofillzone) {
		BufferedImage img = duplicate(mask);
		final int width = img.getWidth(), height = img.getHeight();
		int[] imgdata = new int[width];
		for(int y = 0; y < height; y++) {
			img.getRGB(0, y, width, 1, imgdata, 0, 1);
			for(int x = 0; x < width; x++) {
				if(!nofillzone.contains(x, y)) {
					imgdata[x] = 0xFFFFFFFF;
				} else {
					imgdata[x] = ~imgdata[x];
				}
			}
			img.setRGB(0, y, width, 1, imgdata, 0, 1);
		}
		return img;
	}
	public static BufferedImage notInRect(BufferedImage img, Rectangle rect) {
		int[] imgdata = new int[rect.width];
		for(int y = rect.y; y < rect.y+rect.height; y++) {
			img.setRGB(rect.x, y, rect.width, 1, imgdata, 0, rect.width);
		}
		return img;
	}
	public static BufferedImage whiteSlice(int width, int height, int quad) {
		BufferedImage out = newBI(width, height);
		Graphics g = out.getGraphics();
		g.setColor(Color.WHITE);
		int[] xpos=null, ypos=null;
		switch(quad) {
		case 0:
			xpos = new int[] {0, width/2-1, width-1};
			ypos = new int[] {0, height/2, 0};
			break;
		case 1:
			xpos = new int[] {width, width/2, width};
			ypos = new int[] {-1, height/2-1, height-1};
			break;
		case 2:
			xpos = new int[] {width+1, width/2, 0};
			ypos = new int[] {height, height/2, height};
			break;
		case 3:
			xpos = new int[] {0, width/2, 0};
			ypos = new int[] {0, height/2, height};
			break;
		}
		g.fillPolygon(xpos, ypos, 3);
		g.dispose();
		return out;
	}
	public static BufferedImage rotated90(Image in) {
		BufferedImage out = newBI(in.getHeight(null), in.getWidth(null));
		Graphics2D g = out.createGraphics();
		AffineTransform at = new AffineTransform();
		at.translate(in.getHeight(null), 0);
		at.rotate(Math.PI / 2);
		g.drawImage(in, at, null);
		g.dispose();
		return out;
	}
	public static BufferedImage flippedHoriz(Image in) {
		BufferedImage out = newBI(in.getWidth(null), in.getHeight(null));
		Graphics2D g = out.createGraphics();
		AffineTransform at = new AffineTransform();
		at.translate(in.getWidth(null), 0);
		at.scale(-1, 1);
		g.drawImage(in, at, null);
		g.dispose();
		return out;
	}
	
	// Neighborhood, as indices in the neighbors array:
	// 0 1 2
	// 7 X 3
	// 6 5 4
	public BufferedImage getTile(boolean[] neighbors) {
		if(neighbors.length == 4) {
			boolean[] old = neighbors;
			neighbors = new boolean[8];
			for(int i = 0; i < 4; i++) {
				neighbors[i*2+1] = old[i];
			}
			for(int i = 0; i < 4; i++) {
				if(old[i==0?3:(i-1)] && old[i]) {
					neighbors[i*2] = true;
				}
			}
		}
		BufferedImage out = duplicate(mainImg);
		Graphics2D g = out.createGraphics();
		g.setBackground(new Color(0, true));
		for(int i = 3; i >= 0; i--) {
			boolean solid = neighbors[i*2+1];
			if(!solid) {
				Rectangle al = alRects[i];
				boolean doAl = alLines[i] != -1;
				if(i == 0 && topperLayer) {
					if(doAl) {
						g.clearRect(al.x, al.y, al.width, al.height);
					}
					g.drawImage(toppers[i], 0, 0, null);
					continue;
				}
				boolean prev = neighbors[(i*2+7)%8];
				boolean next = neighbors[(i*2+3)%8];
				if(i==1&&topperLayer) {
					prev = true;
				} else if(i==3&&topperLayer) {
					next = true;
				}
				if(prev && next) {
					if(doAl) {
						g.clearRect(al.x, al.y, al.width, al.height);
					}
					g.drawImage(toppers[i], 0, 0, null);
				} else {
					if(doAl) {
						g.dispose();
						applyMask(out, invertMask(diaMasks[i], al));
						if(prev) {
							applyMask(out, invertMask(diaMasks[(i+3)%4], al));
						}
						if(next) {
							applyMask(out, invertMask(diaMasks[(i+1)%4], al));
						}
						g = out.createGraphics();
						g.setBackground(new Color(0, true));
					}
					g.drawImage(masked(toppers[i], diaMasks[i]), 0, 0, null);
					if(prev) {
						g.drawImage(masked(toppers[i], diaMasks[(i+3)%4]), 0, 0, null);
					}
					if(next) {
						g.drawImage(masked(toppers[i], diaMasks[(i+1)%4]), 0, 0, null);
					}
				}
			}
		}
		boolean[] cutcorners = new boolean[4];
		for(int i = 0; i < 4; i++) {
			boolean solid = neighbors[i*2];
			boolean prev = neighbors[(i*2+7)%8];
			boolean next = neighbors[(i*2+1)%8];
			if(!solid) {
				if(prev && next) {
					if(alLines[i] != -1 && alLines[(i+3)%4] != -1) {
						Rectangle in = alRects[i].intersection(alRects[(i+3)%4]);
						g.clearRect(in.x, in.y, in.width, in.height);
					}
					g.drawImage(corners[i], 0, 0, null);
				}
			}
			if(!prev && !next) {
				cutcorners[i] = true;
			}
		}
		if(useMask) {
			final int w = out.getWidth()/2;
			final int h = out.getHeight()/2;
			Rectangle[] rects = new Rectangle[] {
					new Rectangle(0,0,w,h),
					new Rectangle(w,0,w,h),
					new Rectangle(w,h,w,h),
					new Rectangle(0,h,w,h)
			};
			for(int i = 0; i < 4; i++) {
				if(cutcorners[i]) {
					applyMask(out, mask, rects[i]);
				}
			}
		}
		return out;
	}
	public BufferedImage getTile(int neighbors, int amt) {
		boolean[] neigh = new boolean[amt];
		for(int i = 0; i < amt; i++) {
			neigh[amt-i-1] = (neighbors & 1) > 0;
			neighbors >>= 1;
		}
		return getTile(neigh);
	}
	public static Graphics bgImageFill(Graphics g, Image img, int width, int height) {
		int w = img.getWidth(null); int h = img.getHeight(null);
		for(int x = 0; x < width; x += w) {
			for(int y = 0; y < height; y += h) {
				g.drawImage(img, x, y, null);
			}
		}
		return g;
	}
	public Graphics fillBG(Graphics g, int width, int height) {
		if(bgColor != null) {
			g.setColor(bgColor);
			g.fillRect(0, 0, width, height);
		} else if(bgImage != null) {
			bgImageFill(g, bgImage, width, height);
		}
		return g;
	}
	public BufferedImage tileset(int amt, int width) {
		final int iw = mainImg.getWidth();
		final int ih = mainImg.getHeight();
		final int ow = width*iw;
		final int oh = (int) Math.ceil(((float) amt)/(float) width)*ih;
		BufferedImage out = newBI(ow, oh);
		Graphics g = out.getGraphics();
		fillBG(g, ow, oh);
		if(amt == 16) {
			for(int i = 0; i < 16; i++) {
				g.drawImage(getTile(i,4), (i%width)*iw, (i/width)*ih, null);
			}
		} else if(amt == 256) {
			for(int i = 0; i < 256; i++) {
				g.drawImage(getTile(i,8), (i%width)*iw, (i/width)*ih, null);
			}
		} else if(amt == 48) {
			for(int i = 0; i < 47; i++) {
				int t = tiles48[i];
				while(t == 0 && i != 30) {
					try { Thread.sleep(5);
					} catch (InterruptedException e) {  }
					t = tiles48[i];
				}
				g.drawImage(getTile(t,8), (i%width)*iw, (i/width)*ih, null);
			}
		}
		return out;
	}
	public BufferedImage level(String[] l0) {
		String[] l = new String[l0.length];
		for(int i = 0; i < l.length; i++) {
			StringBuilder sb = new StringBuilder();
			String[] s = l0[i].split(",");
			if(s.length == 1) {
				l[i] = l0[i].replaceAll("(?=.)(?=[^01])","");
			} else {
				for(int j = 0; j < s.length; j++) {
					String sj = s[j].trim();
					if(sj.length() == 0) continue;
					if(sj.equals("0")) {
						sb.append("0");
					} else {
						sb.append("1");
					}
				}
				l[i] = sb.toString();
			}
		}
		final int width = l[0].length();
		final int height = l.length;
		final int iw = mainImg.getWidth();
		final int ih = mainImg.getHeight();
		final int ow = width*iw;
		final int oh = height*ih;
		BufferedImage out = newBI(ow, oh);
		Graphics g = out.getGraphics();
		fillBG(g, ow, oh);
		for(int x = 0; x < width; x++) {
			for(int y = 0; y < height; y++) {
				if(l[y].charAt(x) == '1') {
					boolean[] neighbors = new boolean[] {
							(x==0||y==0)?true:l[y-1].charAt(x-1)=='1',
							(y==0)?true:l[y-1].charAt(x)=='1',
							(x==width-1||y==0)?true:l[y-1].charAt(x+1)=='1',
							(x==width-1)?true:l[y].charAt(x+1)=='1',
							(x==width-1||y==height-1)?true:l[y+1].charAt(x+1)=='1',
							(y==height-1)?true:l[y+1].charAt(x)=='1',
							(x==0||y==height-1)?true:l[y+1].charAt(x-1)=='1',
							(x==0)?true:l[y].charAt(x-1)=='1'
					};
					g.drawImage(getTile(neighbors), x*iw, y*ih, null);
				}
			}
		}
		g.dispose();
		return out;
	}
	public static boolean checkAllAlpha(int[] data) {
		for(int i = 0; i < data.length; i++) {
			byte dat = (byte) (data[i] >> 24);
			if(dat != -1) {
				return false;
			}
		}
		return true;
	}
	public void calculateAlphaLines() {
		final int width = mainImg.getWidth();
		final int height = mainImg.getHeight();
		alTop = -1;
		int[] data = new int[width];
		for(int y = height/2; y >= 0; y--) {
			topper.getRGB(0, y, width, 1, data, 0, width);
			if(checkAllAlpha(data)) {
				alTop = y;
			}
		}
		alBot = -1;
		for(int y = (height-height/2)+1; y < height; y++) {
			bottom.getRGB(0, y, width, 1, data, 0, width);
			if(checkAllAlpha(data)) {
				alBot = y;
			}
		}
		alLft = -1;
		data = new int[height];
		for(int x = width/2; x >= 0; x--) {
			left.getRGB(x, 0, 1, height, data, 0, 1);
			if(checkAllAlpha(data)) {
				alLft = x;
			}
		}
		alRgt = -1;
		for(int x = (width-width/2)+1; x < width; x++) {
			right.getRGB(x, 0, 1, height, data, 0, 1);
			if(checkAllAlpha(data)) {
				alRgt = x;
			}
		}
		alLines = new int[] {alTop, alRgt, alBot, alLft};
		alRects = new Rectangle[] {new Rectangle(0,0,width,alTop),new Rectangle(alRgt,0,width-alRgt,height),
				new Rectangle(0,alBot,width,height-alBot), new Rectangle(0,0,alLft,height)
		};
	}
	public static BufferedImage newBI(int w, int h) {
		return new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
	}
	public BufferedImage makeCorner(int which) {
		final int width = mainImg.getWidth(), height = mainImg.getHeight();
		BufferedImage out = newBI(width, height);
		Graphics g = out.getGraphics();
		int which2 = (which+3)%4;
		//g.drawImage(masked(toppers[which], diaMasks[which]), 0, 0, null);
		//g.drawImage(masked(toppers[which2], diaMasks[which2]), 0, 0, null);
		g.drawImage(masked(toppers[which2], diaMasks[which]), 0, 0, null);
		g.drawImage(masked(toppers[which], diaMasks[which2]), 0, 0, null);
		return out;
	}
	public void makeCorners() {
		corners = new BufferedImage[4];
		for(int i = 0; i < 4; i++) {
			corners[i] = makeCorner(i);
		}
	}
}
