// Copyright (C) 2014  Pitzik4
// Want to see the full notice of legal stuff? It's in notice.txt.

package net.pitzik4.gotile;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import gnu.getopt.Getopt;
import gnu.getopt.LongOpt;

public class GoTile {
	private static final Logger LOGGER = Logger.getLogger(GoTile.class.getName());
	
	public static final String VERSTRING = "1.7";
	public static final int VERSION = 17;
	
	public static void printVersion() {
		LOGGER.info("GoTile version " + VERSTRING);
	}
	public static void printUsage() {
		LOGGER.info("Usage coming sometime.");
	}
	public static void printHelp() {
		printUsage();
		LOGGER.info("Help coming sometime.");
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if(args.length == 0) {
			javax.swing.SwingUtilities.invokeLater(new Runnable() {
	            public void run() {
	            	GoTileUI ui = new GoTileUI();
	    			ui.setVisible(true);
	            }
	        });
			while(true) {
				try {
					Thread.sleep(1000000);
				} catch (InterruptedException e) {  }
			}
		}
		
		LongOpt[] longopts = new LongOpt[9];
		longopts[0] = new LongOpt("help", LongOpt.NO_ARGUMENT, null, 'h');
		longopts[1] = new LongOpt("usage", LongOpt.NO_ARGUMENT, null, 'u');
		longopts[2] = new LongOpt("version", LongOpt.NO_ARGUMENT, null, 'v');
		longopts[3] = new LongOpt("arc", LongOpt.REQUIRED_ARGUMENT, null, 'a');
		longopts[4] = new LongOpt("tiles", LongOpt.REQUIRED_ARGUMENT, null, 't');
		longopts[5] = new LongOpt("shape", LongOpt.REQUIRED_ARGUMENT, null, 's');
		longopts[6] = new LongOpt("level", LongOpt.REQUIRED_ARGUMENT, null, 'l');
		longopts[7] = new LongOpt("format", LongOpt.REQUIRED_ARGUMENT, null, 'f');
		longopts[7] = new LongOpt("topover", LongOpt.NO_ARGUMENT, null, 'p');
		Getopt opts = new Getopt("GoTile", args, "huva:t:s:l:f:p", longopts);
		
		String sarc = "0";
		String stiles = "48";
		String sshape = "square";
		String level = null;
		String format = "PNG";
		boolean topperLayer = false;
		
		int c; while((c = opts.getopt()) != -1) {
			switch(c) {
			case 'h': printHelp(); return;
			case 'u': printUsage(); return;
			case 'v': printVersion(); return;
			case 'a': sarc = opts.getOptarg(); break;
			case 't': stiles = opts.getOptarg(); break;
			case 's': sshape = opts.getOptarg(); break;
			case 'l': level = opts.getOptarg(); break;
			case 'f': format = opts.getOptarg(); break;
			case 'o': topperLayer = true; break;
			}
		}
		
		int arc = Integer.parseInt(sarc);
		int tiles = Integer.parseInt(stiles);
		
		if(tiles != 16 && tiles != 48 && tiles != 256) {
			LOGGER.severe("" + tiles + " is not a supported number of tiles. Defaulting to 48.");
			tiles = 48;
		}
		
		int shape = 0;
		if(sshape.equalsIgnoreCase("square")) {
			switch(tiles) {
			case 16: shape = 4; break;
			case 48: shape = 8; break;
			case 256: shape = 16; break;
			}
		} else {
			shape = Integer.parseInt(sshape);
		}
		
		String[] nargs = new String[args.length-opts.getOptind()];
		for(int i = 0; i < nargs.length; i++) {
			nargs[i] = args[i+opts.getOptind()];
		}
		
		String outfile = "";
		BufferedImage img1 = null, topper = null, mask = null;
		BufferedImage bottom = null, right = null, left = null;
		if(nargs.length < 3) {
			LOGGER.severe("At least 3 arguments are required!");
			System.exit(1);
		}
		try {
			img1 = ImageIO.read(new File(nargs[0]));
			topper = ImageIO.read(new File(nargs[1]));
			if(nargs.length == 4) {
				mask = ImageIO.read(new File(nargs[2]));
			} else if(nargs.length >= 6) {
				right = ImageIO.read(new File(nargs[2]));
				bottom = ImageIO.read(new File(nargs[3]));
				left = ImageIO.read(new File(nargs[4]));
				if(nargs.length >= 7) {
					mask = ImageIO.read(new File(nargs[5]));
				}
			}
			outfile = nargs[nargs.length-1];
		} catch (IOException e) {
			LOGGER.severe("Could not open image file!");
			System.exit(1);
		}
		SheetBuilder sb = SheetBuilder.makeSheetBuilder(img1, topper, bottom, right, left, mask, arc, null, topperLayer);
		BufferedImage out = null;
		if(level == null) {
			out = sb.tileset(tiles, shape);
		} else {
			try {
				out = sb.level(Files.readAllLines(Paths.get(level), Charset.defaultCharset()).toArray(new String[0]));
			} catch (IOException e) {
				LOGGER.severe("Could not open level file!");
				System.exit(1);
			}
		}
		try {
			ImageIO.write(out, format, new File(outfile));
		} catch (IOException e) {
			LOGGER.severe("Could not export image!");
			System.exit(1);
		}
	}
}
