// Copyright (C) 2014  Pitzik4
// Want to see the full notice of legal stuff? It's in notice.txt.

package net.pitzik4.gotile;

import javax.swing.JPanel;
import javax.swing.JLabel;
import com.jgoodies.forms.factories.DefaultComponentFactory;

import java.awt.Component;
import java.awt.Desktop;
import java.awt.Font;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
//import javax.swing.border.EmptyBorder;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UpdateWindow extends JPanel {
	private static final long serialVersionUID = 8542088617268906558L;
	private static final Logger LOGGER = Logger.getLogger(UpdateWindow.class.getName());
	
	private JFrame frame;
	
	public UpdateWindow() {
		JLabel line1 = DefaultComponentFactory.getInstance().createLabel("A new version of GoTile is available, but I'm too lazy to make the");
		line1.setFont(new Font("Dialog", Font.PLAIN, 12));
		
		JLabel line2 = DefaultComponentFactory.getInstance().createLabel("program update itself, so want to go to the website and download it");
		line2.setFont(new Font("Dialog", Font.PLAIN, 12));
		
		JLabel line3 = DefaultComponentFactory.getInstance().createLabel("yourself instead?");
		line3.setFont(new Font("Dialog", Font.PLAIN, 12));
		
		JButton btnYeah = new JButton("Yeah, gimme an update!");
		btnYeah.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(Desktop.isDesktopSupported()) {
					try {
						Desktop.getDesktop().browse(new URI("http://www.pitzik4.net/program/"));
					} catch (IOException | URISyntaxException exc) {
						LOGGER.severe(exc.getMessage());
						LOGGER.severe("Could not open link for some reason!");
					}
				} else {
					LOGGER.warning("Could not open link, desktop not supported.");
				}
				frame.setVisible(false);
				frame.dispose();
			}
		});
		
		JButton btnNoWay = new JButton("These notifications are SO ANNOYING!");
		btnNoWay.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				frame.setVisible(false);
				frame.dispose();
			}
		});
		
		JLabel lblyeahButIll = DefaultComponentFactory.getInstance().createLabel("(Yeah, but I'll still show it to you every time.)");
		lblyeahButIll.setFont(new Font("Dialog", Font.ITALIC, 12));
		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(line1)
						.addComponent(line2)
						.addComponent(line3))
					.addContainerGap(10, Short.MAX_VALUE))
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
					.addContainerGap(321, Short.MAX_VALUE)
					.addComponent(btnYeah)
					.addContainerGap())
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
					.addContainerGap(321, Short.MAX_VALUE)
					.addComponent(btnNoWay)
					.addContainerGap())
				.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
					.addContainerGap(302, Short.MAX_VALUE)
					.addComponent(lblyeahButIll)
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(line1)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(line2)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(line3)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnYeah)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnNoWay)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblyeahButIll)
					.addContainerGap(148, Short.MAX_VALUE))
		);
		setLayout(groupLayout);
		//setBorder(new EmptyBorder(8,8,8,8));
	}
	
	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
	
	public void openIfYouWant(Component relative) {
		try {
			URL url = new URL("http://www.pitzik4.net/gotilevers");
			URLConnection con = url.openConnection();
			Pattern p = Pattern.compile("text/html;\\s+charset=([^\\s]+)\\s*");
			Matcher m = p.matcher(con.getContentType());
			/* If Content-Type doesn't match this pre-conception, choose default and 
			 * hope for the best. */
			String charset = m.matches() ? m.group(1) : "ISO-8859-1";
			Reader r = new InputStreamReader(con.getInputStream(), charset);
			StringBuilder buf = new StringBuilder();
			while (true) {
				int ch = r.read();
				if (ch < 0)
					break;
				buf.append((char) ch);
			}
			if(Integer.parseInt(buf.toString()) > GoTile.VERSION) {
				frame = new JFrame("GoTile - Update Available");
				frame.add(this);
				frame.pack();
				frame.setLocationRelativeTo(relative);
				frame.setVisible(true);
			} else {
				LOGGER.info("Up-to-date!");
			}
		} catch(Exception e) {
			LOGGER.warning("Your computer slapped me when I tried to check for updates.");
		}
	}
}
