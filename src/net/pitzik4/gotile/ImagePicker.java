// Copyright (C) 2014  Pitzik4
// Want to see the full notice of legal stuff? It's in notice.txt.

package net.pitzik4.gotile;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;

public class ImagePicker extends MouseAdapter {
	private static final Logger LOGGER = Logger.getLogger(ImagePicker.class.getName());
	public static final HashSet<String> imgExts = new HashSet<String>(Arrays.asList(ImageIO.getReaderFileSuffixes()));
	
	public static JFileChooser fchooser = new JFileChooser();
	{
		fchooser.setFileFilter(new ImageFileFilter());
	}
	public JButton button;
	public BufferedImage img, icon;
	public ImageIcon imgicon;
	public String imgloc = "";
	private String buttonText;
	private GoTileUI ui;
	
	public ImagePicker(GoTileUI ui) {
		this.ui = ui;
		icon = new BufferedImage(96,112,BufferedImage.TYPE_INT_ARGB);
		imgicon = new ImageIcon(icon);
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		if(e.getButton() == MouseEvent.BUTTON1) {
			if(fchooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
				loadFile(fchooser.getSelectedFile());
			}
		} else if(e.getButton() == MouseEvent.BUTTON3) {
			clearImg();
		}
	}
	public void clearImg() {
		Graphics2D g = icon.createGraphics();
		g.setComposite(AlphaComposite.Clear);
		g.fillRect(0, 0, 96, 96);
		g.dispose();
		img = null;
		imgloc = "";
		button.setIcon(null);
		button.setText(buttonText);
	}
	public ImagePicker setButton(JButton button) {
		this.button = button;
		buttonText = button.getText();
		Graphics2D g = icon.createGraphics();
		g.setComposite(AlphaComposite.Clear);
		g.fillRect(0, 96, 96, 16);
		g.setComposite(AlphaComposite.SrcOver);
		g.setColor(button.getForeground());
		g.setFont(button.getFont());
		Rectangle2D bounds = g.getFont().getStringBounds(buttonText, g.getFontRenderContext());
		g.drawString(buttonText, (int) (96 - bounds.getWidth())/2, 108);
		g.dispose();
		return this;
	}
	public void loadFile(String fpath) {
		if(fpath != null && fpath.length() > 0)
			loadFile(new File(fpath));
		else
			clearImg();
	}
	public void loadFile(File f) {
		try {
			img = ImageIO.read(f);
		} catch (IOException exc) {
			LOGGER.severe(exc.getMessage());
			LOGGER.severe("Could not load file " + f.getName() + "!");
			return;
		}
		if(img == null) {
			LOGGER.warning(f.getName() + " is not a valid image file.");
			return;
		}
		imgloc = f.getAbsolutePath();
		ui.arcSlider.setMaximum(Math.min(img.getWidth(), img.getHeight()));
		Graphics2D g = icon.createGraphics();
		g.setComposite(AlphaComposite.Src);
		final int width = img.getWidth(), height = img.getHeight();
		final int pwidth = width>=height?96:(96*width)/height;
		final int pheight = height>=width?96:(96*height)/width;
		g.drawImage(img, (96-pwidth)/2, (96-pheight)/2, pwidth, pheight, null);
		g.dispose();
		if(button != null) {
			button.setIcon(imgicon);
			button.setText(null);
		}
	}
	
	public static class ImageFileFilter extends FileFilter {
		@Override
		public boolean accept(File f) {
			if(f.isDirectory()) return true;
			int ind = f.getName().lastIndexOf('.');
			if(ind < 0) return false;
			String ext = f.getName().substring(ind+1).toLowerCase();
			return imgExts.contains(ext);
		}
		@Override
		public String getDescription() {
			return "Images";
		}
	}
}
