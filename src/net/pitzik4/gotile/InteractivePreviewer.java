// Copyright (C) 2014  Pitzik4
// Want to see the full notice of legal stuff? It's in notice.txt.

package net.pitzik4.gotile;

public class InteractivePreviewer implements Runnable {
	private GoTileUI ui;
	
	public InteractivePreviewer(GoTileUI ui) {
		this.ui = ui;
	}
	
	@Override
	public void run() {
		while(ui.interactive.isSelected()) {
			ui.updatePreview();
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {  }
		}
	}
}
